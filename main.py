#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

import requests


from datetime import datetime
from loguru import logger
from sys import stdout
NIVEAULOG = 200
logger.remove(None)
logger.add("log/log.log",
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>',
           rotation="1 MB",
           level=NIVEAULOG)
logger.add(stdout,
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level><white>{message}</white></level>',
           level=NIVEAULOG)
date_Heure = str(datetime.now().day).zfill(2) + "-" + str(datetime.now().month).zfill(2) + "-" + str(datetime.now().year) + "-" + str(datetime.now().hour) + "-" + str(datetime.now().minute) + "-" + str(datetime.now().second)
heure_debut = 'Debut du script... le ' + date_Heure
logger.info(heure_debut)

proxy = {'http': 'http://gitlab.st.sncf.fr:3128',
         'https': 'http://gitlab.st.sncf.fr:3128'
         }

coro_pays_csv = 'https://www.data.gouv.fr/fr/datasets/r/f4935ed4-7a88-44e4-8f8a-33910a151d42'


def get_coro():
    """recupere le taux de coronavirus des pays
    sauvegarde ds un fichier et retourne la liste sous forme de tableau
    Returns:
        type: tableau

    """
    # try:
    ret = requests.get(coro_pays_csv, allow_redirects=True, proxies=proxy)
    tab = ret.content.decode('utf-8')
    # open('coro_pays.csv', 'w').write(tab)
    tab = list(tab)
    # except Exception:
    #     print('erreur')
    return tab

def get_population(fichier):
    """charge le fichier de popultaion.

    Args:
        fichier: fichier d epopulation

    Returns:
        type: Description of returned object.

    """
    """recupere le taux de population des pays

    Returns:
        type: tableau

    """
    # try:
    ret = requests.get(coro_pays_csv, allow_redirects=True, proxies=proxy)
    open('coro_pays.csv', 'w').write(ret.content.decode('utf-8'))
    # except Exception:
    #     print('erreur')




get_coro()
